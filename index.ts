import "dotenv/config";

import { v4 as uuid } from "uuid";
import { add } from "date-fns";

import { upsertInvite, sendInvite } from "./lib";

const INVITE_ID = uuid();
console.log("INVITE_ID", uuid());

(async () => {
  const invite = await upsertInvite({
    email: "victor@niftylearning.io",
    id: INVITE_ID,

    name: `The Cronofy Meeting ${INVITE_ID}`,
    description: `demo meeting ${INVITE_ID}`,
    location: `home ${INVITE_ID}`,

    startDate: new Date(),
    endDate: add(new Date(), { hours: 8 }),
  });
  await sendInvite(invite);
})()
  .then(() => {
    process.exit();
  })
  .catch((e) => {
    console.error(e);
    process.exit(1);
  });

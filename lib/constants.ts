export const SENDER_EMAIL = "organizer@niftylearning.app";
export const SENDER_NAME = "Nifty Learning Organizer";
export const CALLBACK_URL = (id?: string) =>
  `https://webhook.site/75be35e2-0d6f-4567-922b-86c2996e888a${
    id ? `?id=${id}` : ""
  }`;

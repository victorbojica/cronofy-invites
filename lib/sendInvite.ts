import { SENDER_EMAIL } from "./constants";
import { getTransporter } from "./mailer";

export const sendInvite = async (invite: any) => {
  const transporter = getTransporter();
  const ics = invite.attachments.icalendar;

  let method = "REQUEST"; // method=CANCEL in the case of cancellations

  await transporter.sendMail({
    from: SENDER_EMAIL,
    to: invite.recipient.email,
    subject: "Smart Invite Example",
    text: "Here is your invite",
    attachments: [
      {
        filename: "invite.ics",
        content: ics,
        contentType: `text/calendar; charset=UTF-8; method=${method}`,
        contentDisposition: "inline",
      },
      {
        filename: "invite.ics",
        content: Buffer.from(ics, "base64"),
        contentType: `application/ics; charset=UTF-8; method=${method}`,
        encoding: "base64",
      },
    ],
  });
};

import aws from "aws-sdk";
import nodemailer from "nodemailer";
const ses = new aws.SES({ apiVersion: "2010-12-01" });

export const getTransporter = () => {
  const transporter = nodemailer.createTransport({ SES: { ses, aws } });

  return transporter;
};

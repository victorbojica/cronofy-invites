import axios from "axios";
import { format } from "date-fns";
import { CALLBACK_URL, SENDER_EMAIL, SENDER_NAME } from "./constants";

const cronofyApi = axios.create({
  baseURL: `https://api-de.cronofy.com`,
  headers: {
    Authorization: `Bearer ${process.env.CRONOFY_TOKEN}`,
    "Content-Type": "application/json",
    "Accept-Encoding": "gzip, deflate",
  },
});

const formatDate = (date: Date) => {
  return `${format(date, "yyyy-MM-dd")}T${format(date, "HH:mm:ss")}Z`;
};

export const upsertInvite = async ({
  email,
  id,
  name,
  description,
  location,
  startDate,
  endDate,
}: {
  email: string;
  id: string;
  name: string;
  description: string;
  location: string;
  startDate: Date;
  endDate: Date;
}) => {
  const payload = {
    method: "request",
    recipient: {
      email,
    },
    smart_invite_id: id,
    callback_url: CALLBACK_URL(),
    event: {
      summary: name,
      description,
      start: formatDate(startDate),
      end: formatDate(endDate),
      tzid: "Europe/Bucharest",
      location: {
        description: location,
      },
    },
    organizer: {
      name: SENDER_NAME,
      email: SENDER_EMAIL,
    },
  };

  console.log(payload);

  try {
    const response = await cronofyApi.post("/v1/smart_invites", payload);

    console.log(response.data);

    return response.data;
  } catch (e: any) {
    console.error(JSON.stringify(e.response.data.errors, null, 2));
    throw new Error("Could not build invite");
  }
};
